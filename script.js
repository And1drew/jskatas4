const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";
const kataList = document.getElementById('kataList');

function kata(solution) {
    const newKata = document.createElement("li");
    newKata.textContent = solution
    return kataList.appendChild(newKata);;
}

function kata1() {
    let array = gotCitiesCSV.split(",");
    return kata(array)
}
kata1();

function kata2() {
    let array = bestThing.split(" ")
    return kata(bestThing.split(" "))
}
kata2();

function kata3() {
    let words = gotCitiesCSV;
    for (let i = 0; i < gotCitiesCSV.length; i++) { words = words.replace(",", "; ") }
    return kata(words);
}
kata3();

function kata4() { return kata(lotrCitiesArray.toString()) }
kata4();

function kata5() {
    let words = [];
    for (let i = 0; i < 5; i++) { words.push(lotrCitiesArray[i]) }
    return kata(words);
}
kata5();

function kata6() {
    let words = [];
    for (let i = 0; i < 6; i++) { words.push(lotrCitiesArray[i + 3]) }
    return kata(words);
}
kata6();

function kata7() {
    let words = [];
    for (let i = 2; i < 5; i++) { words.push(lotrCitiesArray[i]) }
    return kata(words);
}
kata7();

function kata8() {
    let words = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
    words.splice(2, 1);
    return kata(words);
}
kata8();

function kata9() {
    let words = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
    words.splice(6, 3);
    return kata(words);
}
kata9();

function kata10() {
    let words = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
    words.splice(2, 1);
    words.splice(2, 0, "Rohan");
    console.log(words)
    return kata(words);
}
kata10();

function kata11() {
    let words = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
    words.splice(5, 1, "Deadest Marshes")
    return kata(words);
}
kata11();

function kata12() { return kata(bestThing.slice(0, 14)) }
kata12();

function kata13() { return kata(bestThing.slice(69)) }
kata13();

function kata14() { return kata(bestThing.slice(23, 38)) }
kata14();

function kata15() { return kata(bestThing.substring(69)) }
kata15();

function kata16() { return kata(bestThing.substring(23, 38)) }
kata16();

function kata17() {return kata(bestThing.indexOf("only"))}
kata17();

function kata18() {return kata(bestThing.indexOf("bit"))}
kata18();

function kata19() {
    let cities = gotCitiesCSV.split(",");
    let doubleVowels = [];
    for (let i = 0; i < cities.length; i++) {
        if (cities[i].indexOf("aa") != -1) { doubleVowels.push(cities[i]) }
        else if (cities[i].indexOf("ee") != -1) { doubleVowels.push(cities[i]) }
        else if (cities[i].indexOf("ii") != -1) { doubleVowels.push(cities[i]) }
        else if (cities[i].indexOf("oo") != -1) { doubleVowels.push(cities[i]) }
    }
    return kata(doubleVowels);
}
kata19();

function kata20() {
    let doubleVowels = [];
    for (let i = 0; i < lotrCitiesArray.length; i++) {
        if (lotrCitiesArray[i].indexOf("or") != -1) { doubleVowels.push(lotrCitiesArray[i]) }
    }
    return kata(doubleVowels);
}
kata20();

function kata21() {
    let solution = [];
    let words = bestThing.split(" ");
    for (let i = 0; i < words.length; i++) {
        let wordsCharacters = words[i].split("");
        if (wordsCharacters[0] == "b") {
            solution.push(words[i]);
        }
    }
    return kata(solution);
}
kata21();

function kata22() {
    let includesMirkwood = "";
    if (lotrCitiesArray.indexOf("Mirkwood") != -1) { includesMirkwood = "yes" }
    else { includesMirkwood = "no" }
    return kata(includesMirkwood);
}
kata22();

function kata23() {
    let includesHollywood = "";
    if (lotrCitiesArray.indexOf("Hollywood") != -1) { includesHollywood = "yes" }
    else { includesHollywood = "no" }
    return kata(includesHollywood);
}
kata23();

function kata24() { return kata(lotrCitiesArray.indexOf("Mirkwood")) }
kata24();

function kata25() {
    let cityString = lotrCitiesArray;
    let solution = [];
    for (let i = 0; i < cityString.length; i++) {
        let cityCharacters = cityString[i].split("");
        for (let j = 0; j < cityCharacters.length; j++) {
            if (cityCharacters[j] == " ") {
                solution.push(cityString[i]);
            }
        }
    }
    return kata(solution)
}
kata25();

function kata26() { return kata(lotrCitiesArray.reverse()) }
kata26();

function kata27() { return kata(lotrCitiesArray.sort()) }
kata27();

function kata28() { return kata(lotrCitiesArray.sort(function (a, b) { return a.length - b.length })) }
kata28();

function kata29() {
    lotrCitiesArray.pop();
    return kata(lotrCitiesArray);
}
kata29();

function kata30() {
    lotrCitiesArray.push("Dead Marshes");
    return kata(lotrCitiesArray);
}
kata30();

function kata31() {
    lotrCitiesArray.shift();
    return kata(lotrCitiesArray);
}
kata31();

function kata32() {
    lotrCitiesArray.unshift("Rhun");
    return kata(lotrCitiesArray);
}
kata32();